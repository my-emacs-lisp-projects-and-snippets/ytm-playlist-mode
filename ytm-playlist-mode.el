;;; ytm-playlist-mode.el --- interfaces with emms-player-mpd to give you some important controls and info -*- lexical-binding: t -*-

;; Author: Erik Lundstedt
;; Maintainer: Erik Lundstedt
;; Version: 0.1
;; Homepage: https://gitlab.com/erik.lundstedt/mpd-c-mode
;; Package-Requires: ((emacs "25.1")))

;; This file is not part of GNU Emacs

;; This software is free and open source under the wonderful Don't be a jerk License
;; Enjoy your free software!

;;; Commentary:
;; interfaces with emms-player-mpd to give you some important controls and info

;;; Code:

(defcustom mpdc-btn-reload  "reload" "How should the buttons look." )
(defcustom mpdc-btn-play-pause "[play-pause]" "How should the buttons look." )
(defcustom mpdc-btn-next  ">>>" "How should the buttons look." )
(defcustom mpdc-btn-prev  "<<<" "How should the buttons look." )

(defcustom mpdc-btn-vol-upp   "[+]" "How should the volume buttons look." )
(defcustom mpdc-btn-vol-down  "[-]" "How should the volume buttons look." )

(defcustom mpdc-next-func #'emms-player-mpd-next "Function to call the next song.")
(defcustom mpdc-prev-func #'emms-player-mpd-previous "Function to call the previous song")
(defcustom mpdc-play-pause-func #'emms-player-mpd-pause "Function to toggle the music")


(defcustom mpdc-play-volume-upp-func #'emms-volume-raise "Function to toggle the music")
(defcustom mpdc-play-volume-down-func #'emms-volume-lower "Function to toggle the music")

(defun mpdc-prev ()
  "Function that calls defcustom function."
(interactive)
       )
(defun mpdc-next ()
  "Function that calls defcustom function."
(interactive)
       )
(defun mpdc-play-pause ()
  "Function that calls defcustom function."
(interactive)
       )
(defun mpdc-volume-up ()
  "Function that calls defcustom function."
(interactive)
       )
(defun mpdc-volume-down ()
  "Function that calls defcustom function."
(interactive)
       )
;;(defun mpdc-)
;;(defun mpdc-)

(defun mpdc()
"Start the mpdc mode."
(interactive)
(split-window-below 49)
(windmove-down)
(switch-to-buffer "mpdc")
(mpdc-mode)
(emms-player-mpd-connect)
(emms-player-mpd-show)
(run-at-time nil 10 (lambda () (with-current-buffer "mpdc" (mpdc-draw))))
)

(define-minor-mode mpdc-mode
  "Comment."
  ;:keymap (let ((map (make-sparse-keymap)))
   ;         (define-key map (kbd "a") #'mpdc-prev)
    ;        (define-key map (kbd "d") #'mpdc-next)
     ;       )
)

(defun printBtn(text func)
  (insert-button
   text
 'face 'button
 'action func
 'help-echo "mouse-2, RET: Follow this link"
 'follow-link t
 )
(insert " ")
)

(defun testBtn()
"test function to see if the button creation works"
  (interactive)
  (printBtn "hello im a button" (lambda (_button)
            (print "hi")
            )
            )
  )

(defun mpdc-draw ()
  "Draws the ui."
(with-current-buffer "mpdc"

    (erase-buffer)
    (printBtn mpdc-btn-prev (lambda(_button)
        (funcall mpdc-prev-func)
        (mpdc-draw)
        ))
    (printBtn mpdc-btn-play-pause (lambda(_button)
        (funcall mpdc-play-pause-func)
        (mpdc-draw)
        ))
    (printBtn mpdc-btn-next (lambda(_button)
        (funcall mpdc-prev-func)
        (mpdc-draw)
        ))
    (printBtn mpdc-btn-reload (lambda(_button)(mpdc-draw)))
    (insert "\n")
    (emms-player-mpd-show :INSERTP "t")

 )
    )





(provide 'mpd-c-mode)

;;; mpd-c-mode.el ends here
